{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Supplementary Information: Holmes *et al.* 2017\n",
    "\n",
    "# 10. Hierarchical treatment intercept"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Populating the interactive namespace from numpy and matplotlib\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/usr/local/lib/python3.5/site-packages/Cython/Distutils/old_build_ext.py:30: UserWarning: Cython.Distutils.old_build_ext does not properly handle dependencies and is deprecated.\n",
      "  \"Cython.Distutils.old_build_ext does not properly handle dependencies \"\n"
     ]
    }
   ],
   "source": [
    "%pylab inline\n",
    "\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import pickle\n",
    "import pystan\n",
    "import scipy\n",
    "import seaborn as sns\n",
    "\n",
    "import tools\n",
    "\n",
    "import warnings\n",
    "warnings.filterwarnings('ignore')\n",
    "\n",
    "sns.set_context('notebook')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Building the model\n",
    "\n",
    "We extend our model to use groupwise (by probe ID) values for $\\gamma_{j[i]}$ and $\\delta_{j[i]}$, but now we only draw the estimate of the intercept $\\gamma_{j[i]}$ from a pooled distribution.\n",
    "\n",
    "This may be the most appropriate model, biologically-speaking, as we are assuming: (i) a similar linear effect due to the control/growth step of the experiment for all *probe IDs*; (ii) a similar offset for all *probe IDs* due to the the passage step; and (iii) unique 'selection' contributions to the relationship due only to the passage/*treatment* step.\n",
    "\n",
    "We therefore construct the following model of the experiment:\n",
    "\n",
    "$$y_i = \\alpha_{j[i]} + \\beta_{j[i]} x_i + \\gamma_{j[i]} t_i + \\delta_{j[i]} t_i x_i + \\epsilon_i$$\n",
    "$$\\alpha_{j[i]} \\sim N(\\mu_{\\alpha}, \\sigma_{\\alpha}^2)$$\n",
    "$$\\beta_{j[i]} \\sim N(\\mu_{\\beta}, \\sigma_{\\beta}^2)$$\n",
    "$$\\gamma_{j[i]} \\sim N(\\mu_{\\gamma}, \\sigma_{\\gamma}^2)$$\n",
    "$$\\sigma_{\\alpha} \\sim U(0, 100)$$\n",
    "$$\\sigma_{\\beta} \\sim U(0, 100)$$\n",
    "$$\\sigma_{\\gamma} \\sim U(0, 100)$$\n",
    "\n",
    "* $y_i$: measured intensity output on the array for probe $i$ (specific to each replicate)\n",
    "* $x_i$: measured intensity input on the array for probe $i$ (specific to each replicate)\n",
    "* $t_i$: 0/1 indicating whether the probe $i$ was measured in a control (0) or treatment (1) experiment\n",
    "* $\\alpha_{j[i]}$: the linear intercept, this is a constant 'offset' for all *output* measurements relative to *input* measurements, but it differs for each *probe ID*; this is drawn from a Normal distribution $\\sim N(\\mu_{\\alpha}, \\sigma_{\\alpha}^2)$\n",
    "* $\\mu_{\\alpha}$: the mean offset for all *probe ID*s\n",
    "* $\\sigma_{\\alpha}$: the standard deviation of the offset for all *probe ID*s - sampled from a Uniform distribution $U(0, 100)$\n",
    "* $\\beta_{j[i]}$: the linear slope, this is the relative change in measured intensity between *input* and *output* intensities *at the probe ID level* - it differs for each *probe ID*; this is drawn from a Normal distribution $\\sim N(\\mu_{\\beta}, \\sigma_{\\beta}^2)$\n",
    "* $\\mu_{\\beta}$: the mean slope for all *probe ID*s\n",
    "* $\\sigma_{\\beta}$: the standard deviation of the slope for all *probe ID*s - sampled from a Uniform distribution $U(0, 100)$\n",
    "* $\\gamma_{j[i]}$: a groupwise estimate of the influence on *output* measured intensity of introducing passage (*treatment*) into the experiment (i.e. an intercept correction)\n",
    "* $\\mu_{\\gamma}$: the mean intercept due to *treatment* for each *probe ID*\n",
    "* $\\sigma_{\\gamma}$: the standard deviation of the offset for each *probe ID* due to the *treatment* - sampled from a Uniform distribution $U(0, 100)$\n",
    "* $\\delta_{j[i]}$: a groupwise estimate of the influence on *output* measured intensity of introducing passage (*treatment*) into the experiment, as a function of the *input* measured intensity (i.e. a slope correction)\n",
    "* $\\epsilon_i$: error in the model prediction for probe $i$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Stan model construction and fit\n",
    "\n",
    "We need to define `data`, `parameters` and our `model` for `Stan`.\n",
    "\n",
    "In the `data` block, we have:\n",
    "\n",
    "* `N`: `int`, the number of data points)\n",
    "* `J`: `int`, the number of unique probe IDs (`J` < `N`)\n",
    "* `probe`: `int[N]`, an index list of probe identities - one index representing six probe measurements (i.e. three control, three treatment) - there are `J` probes\n",
    "* `t`: `vector[N]`, 0/1 control/treatment values for each probe\n",
    "* `x`: `vector[N]`, the input log(intensity) values\n",
    "* `y`: `vector[N]`, the output log(intensity) values\n",
    "\n",
    "In the `parameter` block, we have:\n",
    "\n",
    "* `a`: `real vector[J]`, representative input log(intensity)\n",
    "* `mu_a`: `real`, an unconstrained value to be fit that represents the mean offset for each probe ID\n",
    "* `b`: `real vector[J]`, effect on log(intensity) of passing through the experiment, specific to a probe ID\n",
    "* `mu_b`: `real`, an unconstrained value to be fit that represents the mean slope for each probe ID\n",
    "* `g`: `real vector[J]`, estimate of the influence of treatment on the output measured intensity (offset)\n",
    "* `mu_g`: `real`, an unconstrained value to be fit that represents the mean offset for each probe ID due to *treatment*\n",
    "* `d`: `real vector[J]`, estimate of the influence of treatment on the output measured intensity (slope)\n",
    "* `sigma`: `real<lower=0>`, the error in the prediction\n",
    "* `sigma_a`: `real<lower=0,upper=100>`, standard deviation of the offset per probe ID\n",
    "* `sigma_b`: `real<lower=0,upper=100>`, standard deviation of the slope per probe ID\n",
    "* `sigma_g`: `real<lower=0,upper=100>`, standard deviation of the offset per probe ID due to *treatment*\n",
    "\n",
    "We also define a `transformed parameter`:\n",
    "\n",
    "* `y_hat[i] <- b[probe[i]] * x[i] + a[probe[i]] + g[probe[i]] * t[i] + d[probe[i]] * t[i] * x[i]`: the linear relationship describing $\\hat{y}$, our estimate of experimental output intensity, which is subject to variance `sigma`.\n",
    "\n",
    "We define the model as:\n",
    "\n",
    "$$\\sigma_{\\alpha} \\sim U(0, 100)$$\n",
    "$$\\alpha_{j[i]} \\sim N(\\mu_{\\alpha}, \\sigma_{\\alpha}^2)$$\n",
    "$$\\sigma_{\\beta} \\sim U(0, 100)$$\n",
    "$$\\beta_{j[i]} \\sim N(\\mu_{\\beta}, \\sigma_{\\beta}^2)$$\n",
    "$$\\sigma_{\\gamma} \\sim U(0, 100)$$\n",
    "$$\\gamma_{j[i]} \\sim N(\\mu_{\\gamma}, \\sigma_{\\gamma}^2)$$\n",
    "$$y \\sim N(\\hat{y}, \\sigma^2)$$\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# load clean, normalised, indexed data\n",
    "data = pd.read_csv(\"output/normalised_indexed_array_data.tab\", sep=\"\\t\")  # full dataset\n",
    "# data = pd.read_csv(\"output/reduced_normalised_indexed_array_data.tab\", sep=\"\\t\")  # reduced dataset for test\n",
    "\n",
    "# useful values\n",
    "probe_ids = data['probe'].unique()\n",
    "nprobes = len(probe_ids)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# define unpooled stan model\n",
    "treatment_model = \"\"\"\n",
    "data {\n",
    "  int<lower=0> N;\n",
    "  int<lower=0> J;\n",
    "  int<lower=1, upper=J> probe[N];\n",
    "  vector[N] t;\n",
    "  vector[N] x;\n",
    "  vector[N] y;\n",
    "}\n",
    "parameters {\n",
    "  vector[J] a;\n",
    "  vector[J] b;\n",
    "  vector[J] g;\n",
    "  vector[J] d;\n",
    "  real mu_a;\n",
    "  real mu_b;\n",
    "  real mu_g;  \n",
    "  real<lower=0> sigma;\n",
    "  real<lower=0,upper=100> sigma_a;\n",
    "  real<lower=0,upper=100> sigma_b;\n",
    "  real<lower=0,upper=100> sigma_g;  \n",
    "}\n",
    "transformed parameters{\n",
    "  vector[N] y_hat;\n",
    "\n",
    "  for (i in 1:N)\n",
    "    y_hat[i] = a[probe[i]] + b[probe[i]] * x[i] + g[probe[i]] * t[i] + d[probe[i]] * t[i] * x[i];\n",
    "}\n",
    "model {\n",
    "  sigma_a ~ uniform(0, 100);\n",
    "  a ~ normal(mu_a, sigma_a);\n",
    "\n",
    "  sigma_b ~ uniform(0, 100);\n",
    "  b ~ normal(mu_b, sigma_b);\n",
    "\n",
    "  sigma_g ~ uniform(0, 100);\n",
    "  g ~ normal(mu_g, sigma_g);\n",
    "\n",
    "y ~ normal(y_hat, sigma);\n",
    "}\n",
    "\"\"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# relate python variables to stan variables\n",
    "treatment_data_dict = {'N': len(data),\n",
    "                       'J': nprobes,\n",
    "                       'probe': data['probe_index'] + 1,\n",
    "                       't': data['treatment'],\n",
    "                       'x': data['log_input'],\n",
    "                       'y': data['log_output']}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# run stan fit\n",
    "treatment_fit = pystan.stan(model_code=treatment_model,\n",
    "                            data=treatment_data_dict,\n",
    "                            iter=1000, chains=2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Save model to file, for reuse# Save fit to file, for reuse\n",
    "unpermutedChains = unpooled_fit.extract()\n",
    "\n",
    "unpermutedChains_df = pd.DataFrame([dict(unpermutedChains)])\n",
    "pickle.dump(unpermutedChains_df, open('output/2016-12-05_notebook10.pkl', 'wb'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "ename": "FileNotFoundError",
     "evalue": "[Errno 2] No such file or directory: 'output/2016-12-05_notebook10.pkl'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mFileNotFoundError\u001b[0m                         Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-2-79f2ae8d80e3>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mtreatment_fit\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0mpickle\u001b[0m\u001b[0;34m.\u001b[0m\u001b[0mload\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0mopen\u001b[0m\u001b[0;34m(\u001b[0m\u001b[0;34m'output/2016-12-05_notebook10.pkl'\u001b[0m\u001b[0;34m,\u001b[0m \u001b[0;34m'rb'\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m)\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mFileNotFoundError\u001b[0m: [Errno 2] No such file or directory: 'output/2016-12-05_notebook10.pkl'"
     ]
    }
   ],
   "source": [
    "treatment_fit = pickle.load(open('output/2016-12-05_notebook10.pkl', 'rb'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inspecting the fit\n",
    "\n",
    "Our primary interest now returns to groupwise values, but this time of the effect due to the *treatment* in the experiment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Get fits to alpha, beta, gamma by probe ID\n",
    "alpha_estimates = pd.Series(treatment_fit['a'].mean(0), index=probe_ids)\n",
    "alpha_se = pd.Series(treatment_fit['a'].std(0), index=probe_ids)\n",
    "beta_estimates = pd.Series(treatment_fit['b'].mean(0), index=probe_ids)\n",
    "beta_se = pd.Series(treatment_fit['b'].std(0), index=probe_ids)\n",
    "gamma_estimates = pd.Series(treatment_fit['g'].mean(0), index=probe_ids)\n",
    "gamma_se = pd.Series(treatment_fit['g'].std(0), index=probe_ids)\n",
    "delta_estimates = pd.Series(treatment_fit['d'].mean(0), index=probe_ids)\n",
    "delta_se = pd.Series(treatment_fit['d'].std(0), index=probe_ids)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Inspect the estimates\n",
    "alpha_estimates.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Inspect the estimates\n",
    "beta_estimates.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Inspect the estimates\n",
    "gamma_estimates.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Inspect the estimates\n",
    "delta_estimates.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Intercepts $\\alpha_{j[i]}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Plot means distribution\n",
    "g = sns.boxplot(alpha_estimates)\n",
    "g.set_title(\"Distribution of mean intercept\")\n",
    "g.set_xlabel(\"mean(alpha)\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Plot means distribution\n",
    "g = sns.boxplot(alpha_se)\n",
    "g.set_title(\"Distribution of intercept standard error\")\n",
    "g.set_xlabel(\"se(alpha)\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# errors for intercept outliers\n",
    "tools.plot_threshold_errors(alpha_estimates, alpha_se, 0.3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# errors for intercept outliers\n",
    "tools.plot_threshold_errors(alpha_estimates, alpha_se, -1, upper=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Slopes $\\beta_{j[i]}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Plot means distribution\n",
    "g = sns.boxplot(beta_estimates)\n",
    "g.set_title(\"Distribution of mean slope\")\n",
    "g.set_xlabel(\"mean(beta)\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Plot means distribution\n",
    "g = sns.boxplot(beta_se)\n",
    "g.set_title(\"Distribution of slope standard error\")\n",
    "g.set_xlabel(\"se(beta)\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# errors for intercept outliers\n",
    "tools.plot_threshold_errors(beta_estimates, beta_se, 0.9936)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# errors for intercept outliers\n",
    "tools.plot_threshold_errors(beta_estimates, beta_se, 0.9927, upper=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Intercepts $\\gamma_{j[i]}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Plot means distribution\n",
    "g = sns.boxplot(gamma_estimates)\n",
    "g.set_title(\"Distribution of mean intercept\")\n",
    "g.set_xlabel(\"mean(gamma)\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Plot means distribution\n",
    "g = sns.boxplot(gamma_se)\n",
    "g.set_title(\"Distribution of intercept standard error\")\n",
    "g.set_xlabel(\"se(gamma)\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# errors for intercept outliers\n",
    "tools.plot_threshold_errors(gamma_estimates, gamma_se, 5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# errors for intercept outliers\n",
    "tools.plot_threshold_errors(gamma_estimates, gamma_se, -1, upper=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Slopes $\\delta_{j[i]}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Plot means distribution\n",
    "g = sns.boxplot(delta_estimates)\n",
    "g.set_title(\"Distribution of mean slope\")\n",
    "g.set_xlabel(\"mean(delta)\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Plot means distribution\n",
    "g = sns.boxplot(delta_se)\n",
    "g.set_title(\"Distribution of slope standard error\")\n",
    "g.set_xscale(\"log\")\n",
    "g.set_xlabel(\"se(delta)\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# errors for intercept outliers\n",
    "tools.plot_threshold_errors(delta_estimates, delta_se, 0.1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# errors for intercept outliers\n",
    "tools.plot_threshold_errors(delta_estimates, delta_se, -0.4, upper=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "df_delta = pd.DataFrame({'treatment_mean': delta_estimates, 'treatment_error': delta_se})\n",
    "df_delta.reset_index(inplace=True)\n",
    "df_delta = df_delta.rename(columns={'index': 'probe'})\n",
    "df_delta = pd.merge(data, df_delta, 'inner', ['probe'])\n",
    "df_delta.head()\n",
    "positives_df.to_csv('2016-12-05_df_delta.tab', sep=\"\\t\", index=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "df_delta.plot('probe_index', 'treatment_mean', 'scatter', alpha=0.2);"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "positives_df = df_delta.loc[(df_delta['treatment_mean'] > 0) &\n",
    "                            (df_delta['treatment_mean'] - df_delta['treatment_error'] > 0)]\n",
    "positives = positives_df[['locus_tag', 'probe', 'treatment_mean', 'treatment_error']]\n",
    "positives = positives.sort('locus_tag', ascending=False)\n",
    "positives = positives.drop_duplicates()\n",
    "positives[:175]\n",
    "positives_df.to_csv('2016-12-05_positives.tab', sep=\"\\t\", index=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "negatives_df = df_delta.loc[(df_delta['treatment_mean'] < 0) &\n",
    "                            (abs(df_delta['treatment_mean']) - df_delta['treatment_error'] > 0)]\n",
    "negatives = negatives_df[['locus_tag', 'probe', 'treatment_mean', 'treatment_error']]\n",
    "negatives = negatives.sort('locus_tag', ascending=False)\n",
    "negatives = negatives.drop_duplicates()\n",
    "negatives[:175]\n",
    "negatives_df.to_csv('2016-12-05_negatives.tab', sep=\"\\t\", index=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "# Visually inspect the effect of censoring on distribution\n",
    "tools.plot_input_output_violin(positives_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Visually inspect the effect of censoring on distribution\n",
    "tools.plot_input_output_violin(negatives_df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "positives_df.shape, negatives_df.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "len(positives_df['locus_tag'].unique()), len(negatives_df['locus_tag'].unique())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "positive_lt = positives_df['locus_tag'].unique()\n",
    "positive_lt.sort()\n",
    "positive_lt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "negative_lt = negatives_df['locus_tag'].unique()\n",
    "negative_lt.sort()\n",
    "negative_lt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "sorted(negatives_df[negatives_df['locus_tag'].str.startswith('p')]['locus_tag'].unique())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import pickle"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "allChains = treatment_fit.extract(permuted = True)\n",
    "\n",
    "allChains_df = pd.DataFrame([dict(allChains)])\n",
    "pickle.dump(allChains_df, open('2016-12-05_permutedchains.pkl', 'wb'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "unpermutedChains = treatment_fit.extract()\n",
    "\n",
    "unpermutedChains_df = pd.DataFrame([dict(unpermutedChains)])\n",
    "pickle.dump(unpermutedChains_df, open('2016-12-05_unpermutedchains.pkl', 'wb'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "test = pickle.load(open('2016-12-05_permutedchains.pkl', 'rb'))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "test.head()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
